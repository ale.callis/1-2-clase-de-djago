
from django.contrib import admin
from django.urls import path
from app import views
urlpatterns = [
    path('admin/', admin.site.urls),
    path('',views.index,name="index"),
    path('pelicula/borrar/<int:id>/',views.eliminar,name="eliminar_pelicula"),
    path('pelicula/editar/<int:id>/',views.editar,name="editar_pelicula"),
    path('login/',views.login,name="login"),
    path('logout/',views.logout,name="logout"),
    path('colecciones/crear', views.ColeccionCreateView.as_view())

]
