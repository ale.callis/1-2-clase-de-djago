from django.contrib import admin

# Register your models here.

from .models import Genero,Coleccion

admin.site.register(Genero)
admin.site.register(Coleccion)