from django.db import models

# Create your models here.

class Genero(models.Model):
    nombre = models.CharField(max_length=150)


class Juego(models.Model):
    nombre = models.CharField(
        help_text = "Ingrese el nombre",
        max_length=150,
        verbose_name = "Nombre"
        )
    fecha = models.DateField()
    edad_minima = models.IntegerField()
    genero = models.ForeignKey(Genero, on_delete=models.CASCADE)

    def __str__(self):
        return self.nombre 

class Coleccion(models.Model):
    nombre = models.CharField(max_length=150)
    fecha_creado = models.DateTimeField(auto_now_add=True)
    juegos = models.ManyToManyField(Juego)

    def contar_juegos(self):
        return self.juegos.count()

    def __str__(self):
        return "nombre = {} Juegos = {}".format(self.nombre,self.contar_juegos())