from django.test import TestCase
from .models import Juego
# Create your tests here.
from django.test import Client
class JuegoTestClass(TestCase):
    def setUp(self):
        pass

    def test_crear_juego(self):
        j = Juego.objects.create(nombre="Pokemon GO",edad_minima=10,fecha="2019-09-01")
        self.assertNotEquals(j,None)
    
    def test_buscar_juego(self):
        j = Juego.objects.create(nombre="Pokemon GO",edad_minima=10,fecha="2019-09-01")
        j = Juego.objects.get(id=j.id)
        self.assertEqual(1,j.id)

    def test_todo_sin_datos(self):
        list = Juego.objects.all()
        self.assertEquals(len(list),0)
    
    def test_todos_con_data(self):
        Juego.objects.create(nombre="Pokemon GO",edad_minima=10,fecha="2019-09-01")
        Juego.objects.create(nombre="Pokemon GO",edad_minima=10,fecha="2019-09-01")
        Juego.objects.create(nombre="Pokemon GO",edad_minima=10,fecha="2019-09-01")
        Juego.objects.create(nombre="Pokemon GO",edad_minima=10,fecha="2019-09-01")
        list = Juego.objects.all()
        self.assertGreater(len(list),3)
    
    def test_index_esta_bien(self):
        c = Client()
        respuesta = c.get('/')
        self.assertEquals(respuesta.status_code,200)
        
