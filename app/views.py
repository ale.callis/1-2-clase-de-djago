from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.urls import reverse
#importamos el usuario base de django
from django.contrib.auth.models import User
#de django imporatamos método de login y logout, además de obtener las credenciales
from django.contrib.auth import authenticate
from django.urls import reverse_lazy

#decoradores
from django.contrib.auth.decorators import login_required
from django.contrib.admin.views.decorators import staff_member_required

#login 
from django.contrib.auth import login as log_in
from django.contrib.auth import logout as log_out

from .models import Juego,Genero,Coleccion
# Create your views here.


from django.views.generic import CreateView

def index(request):
    if request.POST:
        nombre = request.POST.get('nombre',False)
        edad_minima = request.POST.get('edad_minima',0)
        fecha = request.POST.get('fecha',False)
        print(nombre,edad_minima,fecha)
        genero = request.POST.get('genero',False)
        j = Juego(nombre=nombre,edad_minima=edad_minima,fecha=fecha,genero=Genero.objects.get(id=genero))
        j.save()
        return HttpResponseRedirect(reverse("index"))
    #select * from juego;
    context = {'titulo':"INICIO",'juegos':Juego.objects.all()}
    context['generos'] = Genero.objects.all()
    return render(request,'index.html',context)

@staff_member_required(login_url="/login/")
@login_required
def eliminar(request,id):
    try:
        j = Juego.objects.get(id=id)
        j.delete()
    except Exception:
        print("ups")
    return HttpResponseRedirect(reverse("index"))

@login_required
def editar(request,id):
    try:
        j = Juego.objects.get(id=id)
        if request.POST:
            nombre = request.POST.get('nombre',False)
            edad_minima = request.POST.get('edad_minima',0)
            fecha = request.POST.get('fecha',False)
            j.nombre = nombre
            j.fecha = fecha
            j.edad_minima = edad_minima
            j.save()
            return HttpResponseRedirect(reverse("index"))
        context = {'juego':j,'juegos':Juego.objects.all()}
        context['generos'] = Genero.objects.all()
        return render(request,'index.html',context)
    except Exception:
        print("ups")
        return HttpResponseRedirect(reverse("index"))

def login(request):
    if request.POST:
        usuario = request.POST.get('usuario',False)
        contrasenia = request.POST.get('contrasenia',False)
        user = authenticate(request=request,username=usuario,password=contrasenia)
        if user is not None:
            log_in(request,user)
            return HttpResponseRedirect(reverse('index'))
    context = {'titulo':"INICIO SESIÓN"}
    return render(request,'login.html',context)

def logout(request):
    log_out(request)
    return HttpResponseRedirect(reverse('login'))

class ColeccionCreateView(CreateView):
    model = Coleccion
    template_name =  'agregar.html'
    fields = ('nombre', 'juegos')
    success_url = reverse_lazy('index')